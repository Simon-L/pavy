#! /bin/sh
echo "Using patch path:" $1

if [ -e "$1/c" ]; then
  echo Found C code at $1/c
else
  echo Heavy code not found at $1/c
  exit 1
fi

mkdir -p heavy

rm heavy/*
cp $1/c/* heavy/

echo "Compiling"
g++ -std=gnu++11 heavy/*.cpp heavy/*.c main.c -D_REENTRANT -lpulse-simple -lpulse -lm -o main

if [ ${?} -eq 0 ]; then
  echo "Done. Running patch."
else
  echo Done. Error during compilation, not running.
  exit 1
fi

./main
