# PaVy

**Heavy patch player using PulseAudio.**

A simple script that compiles and runs Heavy source code, generated from a Pd patch.

Heavy is a framework made by Enzienaudio for creating interactive sound and music for games, instruments or installations. [More information](https://enzienaudio.com)
> This script has been updated for use with local hvcc since its release as open-source: https://github.com/enzienaudio/hvcc


Example usage:  
```
$ ./pavy.sh ~/Bureau/heavycode
Using patch path: /home/user/Bureau/heavycode
Found C code at /home/user/Bureau/heavycode/c
Compiling
Done. Running patch.
Audio channels: 2
```

## Usage

This script is very basic and takes a single argument that *must* be a path to a folder where hvcc-generated code is located, it will pick up the `c` folder automatically.

##### Thoughts

It is assumed as part of *why this repo?* that you are interested in the C/C++ sources rather than plugin sources or precompiled binaries of your patch. Likely, you might want to compile the code again for specific targets afterwards. I made this script for testing patches easily on Linux on my way to running patches on Arduino boards!

You will need **libpulse-dev**, **gcc** and **g++**:  
`sudo apt install build-essential libpulse-dev`

-----------

Powered by ![Powered by Heavy](https://enzienaudio.com/static/img/heavy_logo_prod_wtxt.svg)
